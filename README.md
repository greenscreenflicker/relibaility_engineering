# Reliability engineering


Reliability engineering for power electronic converters.
A tutorial how to validate robustness.

The idea is based on a presentation Mr 

# Power converter circitcal states

Power converters have five major operation states that need to be verified

* Turn On
* Operation at 50%
* Turn off
* Load step (0% to 100%)
* Load drop (100% to 0%)


# Ciritical components

The following voltages have to measured for a basic reliability test

## Mosfet

* Gate Voltage: Min. 20% safety marge
* Drain-Source Voltage Min. 10% safety marge

## Capacitor

* Ripple Current
* Supply voltage

## Switching controllers

* Drain Source Voltage (10% safety)
* Supply Voltage (20% safety)
* Maximum Curent

## Switching diodes
* Reverse Voltage (10% derating)
* forward current (10% derating).

# Resistors
* Inrush resistors
* Shunts

# Pracital example

Assuming a power flyback power converter with integrated switch, the following table would have to be completed

Desciption | Designator | Datasheet URL  | Parameter |  Max. Value | Turn On | Operation 50% | Turn Off | Load step | Load drop |
---------  | ---------- | -------------- | --------- | ----------- | ------- | ------------- | -------- | --------- | ------ |
Flyback-IC | U1         | [Datasheet](https://datasheet.lcsc.com/szlcsc/On-Bright-Elec-OB25133QCPA_C138841.pdf) | Peak Voltage | 750 V | ? | ? | ? | ? | ? |
Flyback-IC | U1         | [Datasheet](https://datasheet.lcsc.com/szlcsc/On-Bright-Elec-OB25133QCPA_C138841.pdf) | Peak Current | Internally limited | - | - | - | - | -|
Flyback Snubber Rectification Diode | D1         | [Datasheet](https://www.onsemi.com/pub/Collateral/ES1J-D.PDF) | Reverse Voltage | 600V | ? | ? | ? | ? | ? |
Primary Rectification Diode | D2         | [Datasheet](https://datasheet.lcsc.com/szlcsc/Shandong-Jingdao-Microelectronics-SS110_C116711.pdf) | Reverse Voltage | 100 V | ? | ? | ? | ? | ? |
Secondary Rectification Diode | D3         | [Datasheet](https://datasheet.lcsc.com/szlcsc/Shandong-Jingdao-Microelectronics-SS110_C116711.pdf) | Reverse Voltage | 100 V | ? | ? | ? | ? | ? |
Auxiliary Diode | D4         | [Datasheet](https://datasheet.lcsc.com/szlcsc/Shandong-Jingdao-Microelectronics-SS110_C116711.pdf) | Reverse Voltage | 100 V | ? | ? | ? | ? | ? |


# Thermal imaging test
The design should be tested by a thermal imaging camera under full load for 1h.
